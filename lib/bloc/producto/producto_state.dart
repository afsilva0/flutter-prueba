part of 'producto_bloc.dart';

abstract class ProductoState extends Equatable {
  @override
  List<Object> get props => [];

  Product producto =
      new Product(disponible: false, fotoUrl: "", titulo: "", valor: 0);

  setProducto(Product product) {
    producto = product;
  }

  getProducto() {
    return producto;
  }
}

class ProductoInitial extends ProductoState {}

class ProductoLoading extends ProductoState {}

class ProductoLoaded extends ProductoState {
  final List<Product> productos;
  ProductoLoaded(this.productos);
}

class ProductLoaded extends ProductoState {
  final Product producto;
  ProductLoaded(this.producto);

/*   Product get getProducto => producto; */
/*  */
  setProducto(producto);
}

class ProductoError extends ProductoState {
  // error
  final String message;
  ProductoError(this.message);

  @override
  List<Object> get props => [message];
}

class ProductoSuccessful extends ProductoState {
  // error
  final String message;
  ProductoSuccessful(this.message);

  @override
  List<Object> get props => [message];
}
