import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:login_flutter/models/product_model.dart';
import 'package:login_flutter/repositories/producto_repository.dart';
part 'producto_event.dart';
part 'producto_state.dart';

class ProductoBloc extends Bloc<ProductoEvent, ProductoState> {
  ProductoRepository productoRepository;
  ProductoBloc({required this.productoRepository}) : super(ProductoInitial());

  @override
  Stream<ProductoState> mapEventToState(ProductoEvent event) async* {
    if (event is GetEvent) {
      try {
        yield ProductoLoading();
        final data = await productoRepository.getProductos();
        yield ProductoLoaded(data);
      } catch (er) {
        yield ProductoError(er.toString());
      }
    }

    if (event is PostProductButtonPressed) {
      try {
        yield ProductoLoading();
        await productoRepository.postProducto(event.produc);

        yield ProductoSuccessful("Guardado exitoso");
      } catch (er) {
        yield ProductoError(er.toString());
      }
    }

    if (event is LoadProductButtonPressed) {
      try {
        yield ProductLoaded(event.produc);
      } catch (er) {
        yield ProductoError(er.toString());
      }
    }

    if (event is PatchProductButtonPressed) {
      try {
        yield ProductoLoading();

        await productoRepository.patchProducto(event.produc);
        yield ProductoSuccessful("Actualización exitosa");
        yield ProductLoaded(event.produc);
      } catch (er) {
        yield ProductoError("Error al  actualizar producto");
      }
    }

    if (event is DeleteProductButtonPressed) {
      try {
        yield ProductoLoading();

        await productoRepository.deleteProducto(event.produc);

        yield ProductoSuccessful("Se elimino correctamente");
      } catch (er) {
        yield ProductoError("Error al  eliminar producto");
      }
    }
  }
}
