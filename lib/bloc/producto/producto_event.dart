part of 'producto_bloc.dart';

abstract class ProductoEvent extends Equatable {
  @override
  List<Object> get props => [];

  Product get getProducto =>
      new Product(disponible: false, fotoUrl: "", titulo: "", valor: 0);
}

class GetEvent extends ProductoEvent {}

class PostProductButtonPressed extends ProductoEvent {
  final Product produc;
  PostProductButtonPressed(this.produc);

  @override
  Product get getProducto => produc;
}

class PatchProductButtonPressed extends ProductoEvent {
  final Product produc;
  PatchProductButtonPressed(this.produc);

  @override
  Product get getProducto => produc;
}

class DeleteProductButtonPressed extends ProductoEvent {
  final Product produc;
  DeleteProductButtonPressed(this.produc);

  @override
  Product get getProducto => produc;
}

class LoadProductButtonPressed extends ProductoEvent {
  final Product produc;
  LoadProductButtonPressed(this.produc);

  @override
  Product get getProducto => produc;
}
