import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:login_flutter/bloc/login/login_bloc.dart';
import 'package:login_flutter/bloc/producto/producto_bloc.dart';
import 'package:login_flutter/models/product_model.dart';
import 'package:login_flutter/pages/home_page.dart';
import 'package:login_flutter/repositories/auth_repository.dart';
import 'package:login_flutter/widgets/widgets.dart';
import 'package:login_flutter/ui/input_decorations.dart';

class UpdateProductPage extends StatelessWidget {
  UpdateProductPage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Productos'),
          leading: IconButton(
            icon: Icon(Icons.login_outlined),
            onPressed: () {
              //authService.logout();
              Navigator.pushReplacementNamed(context, 'login');
            },
          ),
        ),
        body: AuthBackground(
            child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 250),
              BlocListener<ProductoBloc, ProductoState>(
                  listener: (context, state) {
                //print(state);
              }, child: BlocBuilder<ProductoBloc, ProductoState>(
                      builder: (context, state) {
                //print(state.getProducto().toJson());
                return CardContainer(
                    child: Column(
                  children: [
                    SizedBox(height: 10),
                    Text('Producto',
                        style: Theme.of(context).textTheme.headline4),
                    SizedBox(height: 30),
                    _ProductForm()
                  ],
                ));
              })),
              SizedBox(height: 50),
              MaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  disabledColor: Colors.grey,
                  elevation: 0,
                  color: Colors.deepPurple,
                  child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                      child: Text(
                        'Volver',
                        style: TextStyle(color: Colors.white),
                      )),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => HomePage()));
                  }),
              SizedBox(height: 50),
            ],
          ),
        )));
  }
}

class _ProductForm extends StatefulWidget {
  _ProductForm();

  @override
  __ProductFormState createState() => __ProductFormState();
}

class __ProductFormState extends State<_ProductForm> {
  __ProductFormState();

  bool guardando = false;
  AuthRepository? authRepository;

  Product producto =
      new Product(disponible: false, fotoUrl: " ", titulo: " ", valor: 0);

  TextEditingController disponible = TextEditingController();
  TextEditingController urlImagen = TextEditingController();
  TextEditingController titulo = TextEditingController();
  TextEditingController valor = TextEditingController();

  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
        child: BlocListener<ProductoBloc, ProductoState>(
      listener: (context, state) {
        if (state is ProductoError) {
          _showMessenge(context, state.message, Colors.red);
        }

        if (state is ProductoSuccessful) {
          _showMessenge(context, state.message, Colors.green);
        }

        if (state is ProductLoaded) {
          new Future.delayed(new Duration(seconds: 1), () {
            Navigator.pop(context); //pop dialog
          });
        }
      },
      child: BlocBuilder<ProductoBloc, ProductoState>(
        builder: (context, state) {
          producto = state.getProducto();
          return Form(
            key: formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              children: [
                TextFormField(
                  controller: //disponible
                      initialValue(producto.disponible.toString(), disponible),
                  autocorrect: false,
                  keyboardType: TextInputType.text,
                  decoration: InputDecorations.authInputDecoration(
                      hintText: 'true',
                      labelText: 'disponible',
                      prefixIcon: Icons.alternate_email_rounded),
                  //onChanged: (value) {}, //=> loginForm.email = value,
                  validator: (value) {
                    return (value != null) ? null : 'Campo requerido';
                  },
                  onSaved: (value) {
                    producto.disponible = value == "true" ? true : false;
                  },
                ),
                SizedBox(height: 30),
                TextFormField(
                  controller: // urlImagen,
                      initialValue(producto.fotoUrl!, urlImagen),
                  autocorrect: false,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecorations.authInputDecoration(
                      hintText: 'https:imagen.jpg',
                      labelText: 'Url Imagen',
                      prefixIcon: Icons.image),
                  //onChanged: (value) {}, //=> loginForm.email = value,
                  validator: (value) {
                    return (value != null && value.length >= 6)
                        ? null
                        : 'La url debe de ser de minimo 6 caracteres';
                  },
                  onSaved: (value) {
                    producto.fotoUrl = value;
                  },
                ),
                SizedBox(height: 30),
                TextFormField(
                  controller: initialValue(producto.titulo, titulo),
                  autocorrect: false,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecorations.authInputDecoration(
                      hintText: 'Memoria Ram',
                      labelText: 'Titulo producto',
                      prefixIcon: Icons.title),
                  //onChanged: (value) {}, //=> loginForm.email = value,
                  validator: (value) {
                    return (value != null && value.length >= 4)
                        ? null
                        : 'El titulo del producto deber ser de minimo 4 caracteres';
                  },
                  onSaved: (value) {
                    producto.titulo = value!;
                  },
                ),
                SizedBox(height: 30),
                TextFormField(
                  controller: initialValue(producto.valor.toString(), valor),
                  autocorrect: false,
                  keyboardType: TextInputType.number,
                  decoration: InputDecorations.authInputDecoration(
                      hintText: '128.0',
                      labelText: 'Valor',
                      prefixIcon: Icons.price_change),
                  //onChanged: (value) {}, //=> loginForm.email = value,
                  validator: (value) {
                    return (value != null) ? null : 'El campo es requerido';
                  },
                  onSaved: (value) {
                    producto.valor = double.parse(value!);
                  },
                ),
                SizedBox(height: 30),
                // msg,
                BlocBuilder<LoginBloc, LoginState>(
                  builder: (context, state) {
                    return Container(
                        child: Column(children: [
                      MaterialButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        disabledColor: Colors.grey,
                        elevation: 0,
                        color: Colors.deepPurple,
                        child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 15, vertical: 15),
                            child: Text(
                              guardando ? 'Espere' : 'Actualizar producto',
                              style: TextStyle(color: Colors.white),
                            )),
                        onPressed: _updateProduct,
                      ),
                      MaterialButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        disabledColor: Colors.grey,
                        elevation: 0,
                        color: Colors.deepPurple,
                        child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 15, vertical: 15),
                            child: Text(
                              guardando ? 'Espere' : 'Eliminar',
                              style: TextStyle(color: Colors.white),
                            )),
                        onPressed: _deleteProduct,
                      )
                    ]));
                  },
                )
              ],
            ),
          );
        },
      ),
    ));
  }

  TextEditingController initialValue(
      String valor, TextEditingController controller) {
    controller.text = valor;
    return controller;
  }

  void _updateProduct() {
    /* setState(() {
      guardando = true;
    }); */
    FocusScope.of(context).unfocus();
    if (!formKey.currentState!.validate()) {
      print("formulario malo");
      return; // para validar que el formulario cumple con los datos sugeridos
    }
    formKey.currentState!.save();
    formKey.currentState!.reset();
    BlocProvider.of<ProductoBloc>(context)
        .add(PatchProductButtonPressed(producto));

    /* setState(() {
      guardando = false;
    }); */
    _onLoading();
  }

  void _deleteProduct() {
    setState(() {
      guardando = true;
    });

    BlocProvider.of<ProductoBloc>(context)
        .add(DeleteProductButtonPressed(producto));

    setState(() {
      guardando = false;
    });

    Navigator.of(context)
        .push(MaterialPageRoute(builder: (BuildContext ontext) => HomePage()));
  }

  void _showMessenge(BuildContext context, String message, Color color) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      backgroundColor: color,
      content: Text(message),
    ));
  }

  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return Dialog(
          child: new Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              new CircularProgressIndicator(),
              new Text("Loading"),
            ],
          ),
        );
      },
    );
  }
}
