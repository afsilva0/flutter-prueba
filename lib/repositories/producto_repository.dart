import 'dart:convert';
import 'package:login_flutter/models/product_model.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

class ProductoRepository {
  final _baseAPI = 'flutter-varios-c8193-default-rtdb.firebaseio.com';

  final storage = new FlutterSecureStorage();

  Future<List<Product>> getProductos() async {
    final List<Product> products = [];
    final url = Uri.https(_baseAPI, 'productos.json',
        {'auth': await storage.read(key: 'token') ?? ''});
    final resp = await http.get(url);

    final Map<String, dynamic> productsMap = json.decode(resp.body);

    productsMap.forEach((key, value) {
      final tempProduct = Product.fromMap(value);
      tempProduct.id = key;
      products.add(tempProduct);
    });
    return products;
  }

  Future postProducto(Product product) async {
    final url = Uri.https(_baseAPI, 'productos.json',
        {'auth': await storage.read(key: 'token') ?? ''});

    var body = json.encode(product.toMap());
    await http.post(url, body: body);
  }

  Future patchProducto(Product product) async {
    final url = Uri.https(_baseAPI, 'productos.json',
        {'auth': await storage.read(key: 'token') ?? ''});

    var body = json.encode(product.toMapUpdate());
    await http.patch(url, body: body);
  }

  Future deleteProducto(Product product) async {
    final url = Uri.https(_baseAPI, "productos/" + product.id! + '.json',
        {'auth': await storage.read(key: 'token') ?? ''});
    await http.delete(url);
  }
}
